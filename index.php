<?php

if(isset($_GET['erro'])){
    $erro = 'erro';
    // var_dump($erro);
}else{
    $erro = '';
}

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./assets/styles/home.css">
  <title>E-cursos | home</title>
</head>

<body>
    <header class="header">
        <div class="header-container">
            <div class="header-title">
                <h1 class="fs-45"><a href="#" class="fw-600 c-white ptb-20">E-cursos</a></h1>
            </div>
            <nav class="header-nav">
                <ul>
                    <li><a href="#" class="ml-20 ptb-20 pl-20 c-white fs-15">Cursos</a></li>
                    <li><a href="#" class="ml-20 ptb-20 pl-20 c-white fs-15">Seja um aluno</a></li>
                    <li><a href="#" class="ml-20 ptb-20 pl-20 c-white fs-15">Entrar</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <script type="text/javascript">
        var usuSenIncorreto = '<?php echo $erro ?>';
        window.onload = function () {
            if (usuSenIncorreto === 'erro') {
                document.getElementById('incorreto').removeAttribute('hidden');
            }
        }
    </script>
</body>

</html>